#!/usr/bin/env python
import csv,sys
from os import path

import PySimpleGUI as sg

def make_window(theme):
    sg.theme(theme)

    onlineRetail_layout = [[sg.Text("An increment on the price will be performed according to the following criteria (the header will be skipped)")],
                    [sg.Text("Date: from 2009-12-01 to 2011-12-09")],
                    [sg.Input(key='-DATE-')],
                    [sg.Text("Country"),sg.Button('OnlineRetail list of countries')],
                    [sg.Input(key='-COUNTRY-')],
                    [sg.Radio('Fixed amount', "RadioOnlineRetail", default=True, size=(15,1), k='-RBFixed-'), sg.Radio('Percentage', "RadioOnlineRetail", default=False, size=(15,1), k='-RBPercentage-')],
                    [sg.Input(key='-AMOUNT-')],
                    [sg.Button("OnlineRetail file to be processed")]]
    
    ecommerce_layout=[[sg.Text("An increment on the price will be performed according to the following criteria (the header will be skipped)")],
                    [sg.Text("Date: between 1430622004384 and 1442545187788")],
                    [sg.Input(key='-DATE_ecommerceS-'),sg.Input(key='-DATE_ecommerceE-')],
                    [sg.Text("Visitor iD, choose a number between 1 and 9 to pick users that are multiples of that number.")],
                    [sg.Input(key='-VisitorID-')],
                    [sg.Text("Amount of price increace")],
                    [sg.Radio('Fixed amount', "RadioOnlineEcommerce", default=True, size=(15,1), k='-RBFixedEcommerce-'), sg.Radio('Percentage', "RadioOnlineEcommerce", default=False, size=(15,1), k='-RBPercentageEcommerce-')],
                    [sg.Input(key='-AMOUNTEcommerce-')],
                    [sg.Button("Ecommerce file to be processed")]]
    
    layout = [[sg.Text('Adaptive Fairness elements', size=(38, 1), justification='center', font=("Helvetica", 16), relief=sg.RELIEF_RIDGE, k='-TEXT HEADING-', enable_events=True)]]
    
    layout +=[[sg.TabGroup([[  sg.Tab('onlineRetail', onlineRetail_layout),
                                sg.Tab('ecommerce',ecommerce_layout)
                                ]],
                               
                               key='-TAB GROUP-')]]
              
    return sg.Window('Adaptive Fairness', layout)


def main():
    window = make_window(sg.theme())
    
    while True:
        event, values = window.read(timeout=100)
        if event not in (sg.TIMEOUT_EVENT, sg.WIN_CLOSED):
            print('============ Event = ', event, ' ==============')
            print('-------- Values Dictionary (key=value) --------')
            for key in values:
                print(key, ' = ',values[key])
        if event in (None, 'Exit'):
            print("[LOG] Clicked Exit!")
            break
        elif event == 'OnlineRetail list of countries':
            sg.popup("Australia\nAustria\nBahrain\nBelgium\nBermuda\nBrazil\nCanada\nChannel Islands\nCyprus\nCzech Republic\nDenmark\nEIRE\nEuropean Community\nFinland\nFrance\nFranceInvoiceNo\nGermany\nGreece\nHong Kong\nIceland\nIsrael\nItaly\nJapan\nKorea\nLebanon\nLithuania\nMalta\nNetherlands\nNigeria\nNorway\nPoland\nPortugal\nRSA\nSaudi Arabia\nSingapore\nSpain\nSweden\nSwitzerland\nThailand\nUnited Arab Emirates\nUnited Kingdom\nUnited KingdomInvoice\nUnspecified\nUSA\nWest Indies")
        elif event == "OnlineRetail file to be processed":
            header='Invoice,"Customer ID",InvoiceDate,Country,StockCode,Description,Price,Quantity,Modified\n'
            folder_or_file = sg.popup_get_file('Choose your file')
            path = '/'.join(str(folder_or_file).split('/')[:-1])
            with open(folder_or_file,'r') as data:
                csvContent = csv.reader(data, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                for _ in range(1):
                    next(data)
                for line in csvContent:
                    price=float(line[6])
                    modified=0
                    if values['-DATE-'] and values['-DATE-'] in line[2]:
                        modified=1
                        if values['-RBFixed-']:
                            price = float(line[6])+float(values['-AMOUNT-'])
                        if values['-RBPercentage-']:
                            percent=price/100
                            price = float(line[6])+(percent*float(values['-AMOUNT-']))
                    if values['-COUNTRY-'] and values['-COUNTRY-'] in line[3]:
                        modified=1
                        if values['-RBFixed-']:
                            price = float(line[6])+float(values['-AMOUNT-'])
                        if values['-RBPercentage-']:
                            percent=price/100
                            price = float(line[6])+(percent*float(values['-AMOUNT-']))
                    line=('"%s","%s","%s","%s","%s","%s","%s","%s","%s"\n'%(line[0],line[1],line[2],line[3],line[4],line[5],price,line[7],modified))
                    newFile='%s/%s'%(path,'onlineRetailProcessed.csv')
                    write(newFile,line,header)
            sg.popup("Created file: " + newFile)
        elif event == "Ecommerce file to be processed":
            header='visitorid,location,timestamp,event,itemid,price,Modified\n'
            folder_or_file = sg.popup_get_file('Choose your file')
            path = '/'.join(str(folder_or_file).split('/')[:-1])
            newFile='%s/%s'%(path,'ecommerceProcessed.csv')
            with open(folder_or_file,'r') as data:
                csvContent = csv.reader(data, delimiter=',', quotechar='"')
                for _ in range(1):
                    next(data)
                for line in csvContent:#between 1430622004384 and 1442545187788
                    if int(line[0])%int(values['-VisitorID-']) == 0 and int(line[2]) >= int(values['-DATE_ecommerceS-']) and int(line[2]) <= int(values['-DATE_ecommerceE-']):
                        if values['-RBFixedEcommerce-']:
                            write(newFile,'"%s","%s","%s","%s","%s","%s","%s"\n'%(line[0],line[1],line[2],line[3],line[4],float(line[5])+float(values['-AMOUNTEcommerce-']),'1'),header)
                        if values['-RBPercentageEcommerce-']:
                            percent=price/100
                            price = float(line[5])+(percent*float(values['-AMOUNTEcommerce-']))
                            write(newFile,'"%s","%s","%s","%s","%s","%s","%s"\n'%(line[0],line[1],line[2],line[3],line[4],price,'1'),header)
                    else:
                        write(newFile,'"%s","%s","%s","%s","%s","%s","%s"\n'%(line[0],line[1],line[2],line[3],line[4],line[5],'0'),header)
            sg.popup("Created file: " + newFile)

    window.close()
    exit(0)

def write(file,line,header):
    if path.exists(file):
        with open (file,'a') as f:
            f.write(line)
    else:
        with open (file,'w') as f:
            f.write(header)
            f.write(line)

if __name__ == '__main__':
    main()
