from os import path
import csv
import numpy as np

header='itemid,price\n'

def write(file,line):
    if path.exists(file):
        with open (file,'a') as f:
            f.write(line)
    else:
        with open (file,'w') as f:
            f.write(header)
            f.write(line)

def main():
    with open('itemid.csv','r') as data:
        csvContent = csv.reader(data, delimiter=',', quotechar='"')
        for _ in range(1):
            next(data)
        for line in csvContent:
            write('price.csv','%s,%s\n'%(line[0],np.random.normal(100, 25)))

main()
print('Done!')
