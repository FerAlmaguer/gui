from os import path
import csv, sys
import PySimpleGUI as sg

layout = [[sg.Text("Hello from PySimpleGUI")], [sg.Button("OK")]]
window = sg.Window("ecommerce", layout)
header='visitorid,location,timestamp,event,itemid,price,Modified\n'

while True:
    event, values = window.read()
    if event == "OK" or event == sg.WIN_CLOSED:
        break

window.close()

def write(file,line):
    if path.exists(file):
        with open (file,'a') as f:
            f.write(line)
    else:
        with open (file,'w') as f:
            f.write(header)
            f.write(line)

def main():
    with open('location.csv','r') as data:
        csvContent = csv.reader(data, delimiter=',', quotechar='"')
        for _ in range(1):
            next(data)
        for line in csvContent:#between 1430622004384 and 1442545187788
            if int(line[0])%2 == 0 and int(line[2]) >= int(sys.argv[1]) and int(line[2]) <= int(sys.argv[2]):
                write('output.csv','"%s","%s","%s","%s","%s","%s","%s"\n'%(line[0],line[1],line[2],line[3],line[4],float(line[5])+5,'1'))
            else:
                write('output.csv','"%s","%s","%s","%s","%s","%s","%s"\n'%(line[0],line[1],line[2],line[3],line[4],line[5],'0'))
