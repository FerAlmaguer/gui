from os import path
import csv

header='visitorid,location,timestamp,event,itemid,price\n'

def write(file,line):
    if path.exists(file):
        with open (file,'a') as f:
            f.write(line)
    else:
        with open (file,'w') as f:
            f.write(header)
            f.write(line)

def main():
    with open('result.csv','r') as data:
        csvContent = csv.reader(data, delimiter=',', quotechar='"')
        for _ in range(1):
            next(data)
        for line in csvContent:
            write('location.csv','"%s","%s","%s","%s","%s","%s"\n'%(line[0],line[0][-1],line[1],line[2],line[3],line[4]))

main()
print('Done!')
