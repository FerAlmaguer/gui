from os import path
import csv

header='Invoice,"Customer ID",InvoiceDate,Country,StockCode,Description,Price,Quantity,Modified\n'

def write(file,line):
    if path.exists(file):
        with open (file,'a') as f:
            f.write(line)
    else:
        with open (file,'w') as f:
            f.write(header)
            f.write(line)

def main():
    with open('Result.csv','r') as data:
        csvContent = csv.reader(data, delimiter=',', quotechar='"')
        for _ in range(1):
            next(data)
        for line in csvContent:
            write(file,line)

main()
print('Done!')
