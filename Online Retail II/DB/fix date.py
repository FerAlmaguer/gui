import csv

info=[]
info2=[]
def main():
    sniffer = csv.Sniffer()
    separador=","
    with open('merged.csv','r') as txtFile:
        for _ in range(1):
            next(txtFile)
        for line in txtFile:
            data=line.split(separador)
            try:
                info.append('%s,%s,%s,%s,%s,%s,%s,%s\n'%(data[0],data[1],data[2],data[3],data[4].split(" ")[0],data[5],data[6],data[7].strip('\n')))
            except Exception as e:
                info2.append(data)
                print(e, data)
        
    with open('date.csv','w') as merge:
        merge.write('Invoice,StockCode,Description,Quantity,InvoiceDate,Price,Customer ID,Country\n')
        merge.write(''.join(info))
        
    with open('leftOut.csv','w') as merge:
        merge.write('Invoice,StockCode,Description,Quantity,InvoiceDate,Price,Customer ID,Country\n')
        for item in info2:
            merge.write("%s\n" % item)
    
    print('Done!\n')
    
main()
