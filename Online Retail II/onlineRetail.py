import csv, sys
from os import path

header='Invoice,"Customer ID",InvoiceDate,Country,StockCode,Description,Price,Quantity,Modified\n'

def write(file,line):
    if path.exists(file):
        with open (file,'a') as f:
            f.write(line)
    else:
        with open (file,'w') as f:
            f.write(header)
            f.write(line)

def main():
    with open('Result.csv','r') as data:
        csvContent = csv.reader(data, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for _ in range(1):
            next(data)
        for line in csvContent:#python3 onlineRetail.py 2011-12-09 'united kindom'
            if sys.argv[1] in line[2] and sys.argv[2] in line[3]:
                line=('"%s","%s","%s","%s","%s","%s","%s","%s","%s"\n'%(line[0],line[1],line[2],line[3],line[4],line[5],float(line[6])+5,line[7],'1'))
            else:
                line=('"%s","%s","%s","%s","%s","%s","%s","%s","%s"\n'%(line[0],line[1],line[2],line[3],line[4],line[5],line[6],line[7],'0'))
            write('output.csv',line)

main()
print('Done!')
