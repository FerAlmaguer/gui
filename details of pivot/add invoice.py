invoices=[]
data=[]
keys=[]
output=open('output.csv','a')
def main():
    with open('invoice.csv','r') as txtFile:
        for _ in range(1):
            next(txtFile)
        for line in txtFile:
            invoices.append(line.strip("\n"))
            
    x=0
    for datum in invoices:
        keys.append(','.join(invoices[x].split(',')[1:]))
        x+=1
        
    with open('result.csv','r') as txtFile:
        for _ in range(1):
            next(txtFile)
        for line in txtFile:
            data.append(line.strip("\n"))
    
    x=0
    output.write('InvoiceID,"Customer ID",Date,"Customer Age","Customer Gender",Country,State,"Product Category","Sub Category",Product,"Frame Size","Order Quantity","Unit Price"\n')
    for datum in data:
        id=','.join(data[x].split(',',2)[:2])
        if id in keys:
            output.write("%s,%s\n"%(''.join(invoices[keys.index(id)].split(',')[:1]),','.join(data[x].split(',',2))))
        else:
            output.write("error,%s\n"%(','.join(data[x].split(',',2))))
        output.flush()
        x+=1
        
    output.close()
    print('Done!\n')
    
main()
