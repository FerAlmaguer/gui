import os, shutil
import os.path
from os import path

header='orderID,categoryName,lenghtCM,heightCM,widthCM,Volume,weightG,sellerCity,sellerState,sellerLat,sellerLng,shippingLimit,purchaseTime,timeLapse,customerCity,customerState,customerLat,customerLng,distance_in_km,freightValue,cost_per_KM\n'

def write(file,line):
    if path.exists(file):
        with open (file,'a') as f:
            f.write(line)
    else:
        with open (file,'w') as f:
            f.write(header)
            f.write(line)

def clearFolders():
    for folder in ('Distance','Time','Volume','WeightG'):
        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))

def main():
    clearFolders()
    with open('data.csv','r') as data:
        for _ in range(1):
            next(data)
        for line in data:
            timeLapse=line.split(',')[-8]
            file='Time/%s.csv'%(timeLapse)
            write(file,line)
            
            volume=line.split(',')[5]
            file='Volume/%s.csv'%(len(volume))
            write(file,line)
            
            weight=line.split(',')[6]
            file='WeightG/%s.csv'%(len(weight))
            write(file,line)
            
            distance=line.split(',')[-3]
            file='Distance/%s.csv'%(len(distance.split('.')[0]))
            write(file,line)

main()
print('Done!')
