olist_customers_dataset.csv:		says where the customers live.
olist_geolocation_dataset.csv:		says the zip code latitude and longitude.
olist_order_items_dataset.csv:		it has the order_id, product_id, seller_id, shipping information, etc. (no purchase date [main table]). order_id: 8272b63d03f5f79c56e9e4120aec44ef
olist_order_payments_dataset.csv:		payment information.
olist_order_reviews_dataset.csv:		customer reviews including dates when a survey was sent to a customer and the timestamp of its reply.
olist_orders_dataset.csv:			main table (according to the author).
olist_products_dataset.csv:		products list.
olist_sellers_dataset.csv:		seller location information.
product_category_name_translation.csv:	translates the product category name to English.

more info on: https://www.kaggle.com/olistbr/brazilian-ecommerce/home?select=product_category_name_translation.csv